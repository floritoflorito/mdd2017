// aspectRatio = width/height
// shapeFunctions = array of shape functions
function Letter(aspectRatio, shapes) {
  
  this.getAspectRatio = function() {
    return aspectRatio;
  }
  
  this.getShapes = function() {
    return shapes;
  }
  
}
