function Settings() {
  
  this.BRICK_BASE_SIZE = 30;
  this.BRICK_LENGTH_DUAL_EXTENSION_MIN = 0;
  this.BRICK_LENGTH_DUAL_EXTENSION_MAX = 5;
  this.MAX_SPACE_MIN = -15;
  this.MAX_SPACE_MAX = 15;
  
  this.GROW_SHRINK_SPEED_ONE_DIRECTION = 5.0; // units / second
  this.HOLD_PHASE_LENGTH = 2.0;
  
  this.CHILD_SPAWN_CHANCE_PER_FRAME = 1.0;//0.04;
  this.CHILD_SPAWN_TRIES_PER_FRAME = 15;
  
  // important to keep 0, 1, 2 (xyz)
  this.ORIENTATION_X = 0;
  this.ORIENTATION_Y = 1;
  this.ORIENTATION_Z = 2;
  this.ORIENTATION_NAMES = [ "x-axis", "y-axis", "z-axis" ];
  
}
