var SHAPE_TYPE_LINE = "LINE";
var SHAPE_TYPE_FREE_SHAPE = "FREE_SHAPE";

// startPoint, endPoint are percentages of character size
function Line(startPoint, endPoint) {
  this.type = SHAPE_TYPE_LINE;
  this.startPoint = startPoint;
  this.endPoint = endPoint;
  this.toString = function() { 
    return "Line( "+startPoint+" -> "+endPoint+" )";
  }
}


function FreeShape(tightness, points) {
  this.type = SHAPE_TYPE_FREE_SHAPE;
  this.tightness = tightness;
  this.points = points;
}
