
var uniqueID = 0;

function Fucker(_tx, _ty, _tz, _type, _dims, _myColor, _treeLevel, _rotOscilFreq, _rotOscilPhase) {

  var debug = false;
  if (debug) console.log("Fucker()");
  
  this.dims = _dims;
  this.treeLevel = _treeLevel;
  this.children = new Array();
  this.parent = null;
  this.detached = true;
  this.rotOscilFreq = _rotOscilFreq;
  this.rotOscilPhase = _rotOscilPhase;
  this.myColor = _myColor;
  this.type = _type;
  
  if (debug) console.log("- myColor="+this.myColor);
  
  var tx=_tx;
  var ty=_ty;
  var tz=_tz;
  this.id = uniqueID;
  uniqueID++;
  
  var ROT_MIN = 0.001;
  var ROT_MAX = 0.003;
  var rx = random(TWO_PI), 
      ry = random(TWO_PI), 
      rz = random(TWO_PI);
  var drx=0, dry=0, drz=0;
 
  var rotOscillator = new Oscillator(_rotOscilFreq, _rotOscilPhase, SETTINGS.OSCILLATOR_SINEXP, SETTINGS.ROT_OSCIL_EXP);
  var detachOnNextDraw = false;
  var mx=0, my=0, mz=0;
  var accY = 0.1;

  var stackedXyzRotations;
  var readyToRemove = false;

  function postConstructor() {
    if (debug) console.log("- Fucker postConstructor");
    
    var totalSpeedInc = 1.0;
    for (var i=0; i<this.treeLevel; i++) {
      var speedInc = random(SETTINGS.TREE_LEVEL_ROTATION_SPEED_INCREASE_MIN, SETTINGS.TREE_LEVEL_ROTATION_SPEED_INCREASE_MAX);
      totalSpeedInc *= speedInc;
    }
    
    drx = totalSpeedInc*random(ROT_MIN, ROT_MAX)*randomPM(); 
    dry = totalSpeedInc*random(ROT_MIN, ROT_MAX)*randomPM();
    drz = totalSpeedInc*random(ROT_MIN, ROT_MAX)*randomPM();
  }
  postConstructor();
  

  this.update = function() {
    //if (!this.detached) {
    var amp = rotOscillator.update();
    amp = map(amp, 0, 1, SETTINGS.ROT_OSCIL_AMP_MIN, SETTINGS.ROT_OSCIL_AMP_MAX);
    rx += drx*amp;
    ry += dry*amp;
    rz += drz*amp;
    //} else {

    if (this.detached) {
      //println("detached xyz:"+x+" "+y+" "+z);
      my += accY;
      tx += mx;
      ty += my;
      tz += mz;

      mx *= SETTINGS.AIR_DRAG;
      my *= SETTINGS.AIR_DRAG;
      mz *= SETTINGS.AIR_DRAG;


      if (ty>2000) {
        readyToRemove = true;
      }
    }
    //}
    
    
  }

  this.detachOnNextDraw = function() {
    detachOnNextDraw = true;
  }


  /*
  *
  *    DETACH
  *
  */
  
  this.detach = function() {

    console.log("detach()");
    console.log(this.id);

    //return;
    // gather all parents to get total xyz rotation
    var parents = new Array();
    var child = this;
    while (child.parent!=null) {
      parents.push(child.parent);
      child = child.parent;
    }
    console.log(parents.length+" parents");

    // get all rotations in correct order:
    stackedXyzRotations = new Array();
    for (var i=parents.length-1; i>=0; i--) {
      var parent = parents[i];
      stackedXyzRotations.push( [ parent.rx, parent.ry, parent.rz ] );
    }


    // get global pos
    tx = modelX(0, 0, 0);
    ty = modelY(0, 0, 0);
    tz = modelZ(0, 0, 0);

    // set movement
    var dx = tx;//(tx-width/2);
    var dy = ty;//(ty-height/2);
    var dz = tz;
    var offCenter = createVector(dx, dy, dz);
    offCenter.normalize();
    offCenter.mult(SETTINGS.BLAST_SPEED);
    mx = offCenter.x;
    my = offCenter.y;
    mz = offCenter.z;

    // remove child from parent children
    // parent.children.remove(this);
    for ( var i = 0; i < this.parent.children.length-1; i++) { 
      //if ( this.parent.children[i] === this) {
      if ( this.parent.children[i].id == this.id ) {
        this.parent.children.splice(i, 1);
      }
    }

    this.parent = null;
    this.detached = true;
  }
  
  
  /*
  *
  *  END detach()
  *
  */
  
  
  

  function drawIt() {
    fill(_myColor);
    switch (_type) {
    case SETTINGS.FUCKER_TYPE_BOX:
      box(_dims[0], _dims[1], _dims[2]);
      break;
    }
  }

  this.drawAttached = function() {
    push();
    translate(tx, ty, tz);

    //console.log(this.detached);
    if (this.detached) {
      push();
      for (var i=0; i<stackedXyzRotations.length; i++) { 
        var xyzRotation = stackedXyzRotations[i];
        rotateX(xyzRotation[0]);
        rotateY(xyzRotation[1]);
        rotateZ(xyzRotation[2]);
      }
    }

    rotateX(rx);
    rotateY(ry);
    rotateZ(rz);

    drawIt();

    if (this.detached) {
      pop();
    }

    if (detachOnNextDraw) {
      detachOnNextDraw = false;
      this.detach();
    }

    // draw children
    push();
    for (var i=0; i<this.children.length; i++) {
      var child = this.children[i];
      child.drawAttached();
    }
    pop();
    pop();
  }
  
  
  
  

  function getAllChildrenAndSelf() {
    var out = new Array();
    getAllChildrenRecursively(this, out);
    out.push(this);
    return out;
  }

  this.toString = function() {
    return "Fucker ID="+this.id+", children="+this.children.length;
  }
  
  if (debug) console.log("Fucker() end");
}

function getAllChildrenRecursively(parent, out) {
  for (var i=0; i<parent.children.length; i++) {
    var child = parent.children[i];
    out.push(child);
    getAllChildrenRecursively(child, out);
  }
}
