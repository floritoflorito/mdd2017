function Ball(x, y) {

  //console.log("Ball("+x+","+y+")");

  var RADIUS = 5;
  var xm;
  var ym;
  
  if (random(1.0)<SETTINGS.BALL_BLACK_CHANCE) {
    this.col = color(0,0,0,0);
  } else {
    this.col = color(random(255),random(255),random(255));
    while (red(this.col)+green(this.col)+blue(this.col)<400) {
      this.col = color(random(255),random(255),random(255));
    }
  }
  
  var movement = createVector(random(-SETTINGS.BALL_SPEED, SETTINGS.BALL_SPEED), random(-SETTINGS.BALL_SPEED, SETTINGS.BALL_SPEED));
  movement.normalize();
  movement.mult(SETTINGS.BALL_SPEED);
  xm = movement.x;
  ym = movement.y;

  var rolling = false;
  var stopped = false;


  /*
  *
  *  UPDATE
  *
  */


  this.update = function() {
    if (!stopped) {
      ym += SETTINGS.GRAVITY;
      if (rolling) {
        xm *= SETTINGS.ROLL_MOTION_DRAG;
        ym *= SETTINGS.ROLL_MOTION_DRAG;
      }
      var futureX = x + xm;
      var futureY = y + ym;
      if (futureX<0+RADIUS) {
        futureX = -futureX+2*RADIUS;
        xm *= - SETTINGS.BOUNCE_DRAG;
      } else if (futureX>=width-RADIUS) {
        futureX = futureX - (futureX-width+RADIUS);
        xm *= - SETTINGS.BOUNCE_DRAG;
      }
      if (futureY<0+RADIUS) {
        futureY = -futureY+2*RADIUS;
        ym *= - SETTINGS.BOUNCE_DRAG;
      } else if (futureY>=height-RADIUS) {
        futureY = futureY - (futureY-height+RADIUS);
        ym *= - SETTINGS.BOUNCE_DRAG;
      }

      if (abs(ym)<SETTINGS.ROLL_MOTION_THRESHOLD && futureY>height-2*RADIUS) {
        rolling = true;
        ym=0;
      }

      if (rolling && !stopped) {
        var speedSq = xm*xm+ym*ym;
        if (speedSq<SETTINGS.STOP_MOTION_THRESHOLD_SQ) {
          xm = 0;
          ym = 0;
          stopped = true;
        }
      }

      x = futureX;
      y = futureY;
    }
  }
  
  
  
  /*
  *  getDirection()
  */
  
  
  this.getDirection = function() {
    var movement = createVector(xm,ym);
    var direction = movement.heading();
    return direction;
  }
  
  
  
  this.getMovement = function() {
    return { x:xm, y:ym };
  }
  
  
  
  this.getPosition = function() {
    return { x:x, y:y };
  }


  /*
  *
  *  DRAW
  *
  */


  this.drawBall = function() {
    stroke(0);
    if (rolling && !stopped) 
      fill(255,0,0);
    else if (stopped) 
      fill(0);
    else
     fill(this.col);
    ellipse(x, y, RADIUS*2-1, RADIUS*2-1);
  }
}
