function Dot(x,y,balls) {
  
  // choose closest ball
  var ball = null;
  var closestDistance = 999999;
  for (var i=0;i<balls.length;i++) {
    var position = balls[i].getPosition();
    var pos = createVector(position.x, position.y);
    var myPos = createVector(x,y);
    var delta = pos.sub(myPos);
    //console.log(delta);
    var distance = delta.mag();
    if (distance<closestDistance) {
      closestDistance = distance;
      ball = balls[i];
    }
  }
  
  var movement = createVector(random(-1,1),random(-1-1));
  movement.normalize();
  movement.mult(SETTINGS.BALL_SPEED*SETTINGS.PIXEL_SPEED_FROM_BALL_SPEED_FACTOR);
  var xm = movement.x;
  var ym = movement.y;
  
  var col = color(red(ball.col),green(ball.col),blue(ball.col),SETTINGS.PIXEL_ALPHA);
  //console.log(hue(col));
  
  this.update = function() {
    var ballMovement = ball.getMovement();
    var targetXm = ballMovement.x*SETTINGS.PIXEL_SPEED_FROM_BALL_SPEED_FACTOR;
    var targetYm = ballMovement.y*SETTINGS.PIXEL_SPEED_FROM_BALL_SPEED_FACTOR;
    xm = (SETTINGS.PIXEL_DIRECTION_SMOOTH*targetXm) + (1-SETTINGS.PIXEL_DIRECTION_SMOOTH)*xm;
    ym = (SETTINGS.PIXEL_DIRECTION_SMOOTH*targetYm) + (1-SETTINGS.PIXEL_DIRECTION_SMOOTH)*ym;
    x += xm;
    y += ym;
    if (x<0) x+=width; else if (x>=width) x-=width;
    if (y<0) y+=height; else if (y>=height) y-=height;
  }
  
  
  
  this.draw = function() {
    //strokeWeight(2);
    if (!ball.rolling) {
      stroke(col);
      point(x,y);
    }
  }
}
