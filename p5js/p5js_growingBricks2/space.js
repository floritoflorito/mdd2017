//var takenSpaces = new Array(); // array of xyz arrays
//var



function initMaxSpace() {

  maxSpace = new Array();
  for (var x=SETTINGS.MAX_SPACE_MIN; x<=SETTINGS.MAX_SPACE_MAX; x++) {
    var yArr = new Array();
    for (var y=SETTINGS.MAX_SPACE_MIN; y<=SETTINGS.MAX_SPACE_MAX; y++) {
      var zArr = new Array();
      for (var z=SETTINGS.MAX_SPACE_MIN; z<=SETTINGS.MAX_SPACE_MAX; z++) {
        zArr.push(false);
      }
      yArr.push(zArr);
    }
    maxSpace.push(yArr);
  }

  console.log("initMaxSpace()");
  console.log(maxSpace);
}


function getSpacePositions(brick) {
  var pos = brick.getPosition();
  var orientation = brick.getOrientation();
  var singleLength = brick.getSingleDirectionLength();
  var insideLegalSpace = true;
  var allSpacePositions = [];

  for (var a=-singleLength; a<=singleLength; a++) {
    var pos2 = clonePos(pos);
    pos2[orientation] = pos2[orientation] + a;
    var spaceIndex = unitPosToSpacePos(pos2);
    insideLegalSpace = insideLegalSpace && spacePosInRange(spaceIndex);
    allSpacePositions.push(spaceIndex);
  }

  return { 
  insideLegalSpace:
  insideLegalSpace, allSpacePositions:
    allSpacePositions
  };
}


function removeBrickFromSpace(brick) {
  var result = getSpacePositions(brick);
  var allSpacePositions = result.allSpacePositions;
  for (var i=0; i<allSpacePositions.length; i++) {
    var spaceIndex = allSpacePositions[i];
    maxSpace[ spaceIndex[0] ][ spaceIndex[1] ][ spaceIndex[2] ] = false;
  }
}


function addBrickToSpace(brick) {


  var result = getSpacePositions(brick);
  var allSpacePositions = result.allSpacePositions;
  var insideLegalSpace = result.insideLegalSpace;


  if (insideLegalSpace) {

    var allSpacesEmpty = true;
    for (var i=0; i<allSpacePositions.length; i++) {
      var spaceIndex = allSpacePositions[i];
      var singleSpaceEmpty = maxSpace[ spaceIndex[0] ][ spaceIndex[1] ][ spaceIndex[2] ] == false;
      allSpacesEmpty = allSpacesEmpty && singleSpaceEmpty;
      // TODO: FILL ONLY WHEN ALL SPACES ARE EMPTY!!!
    }
    if (allSpacesEmpty) {
      // now actually fill the space index array
      for (var i=0; i<allSpacePositions.length; i++) {
        var spaceIndex = allSpacePositions[i];
        maxSpace[ spaceIndex[0] ][ spaceIndex[1] ][ spaceIndex[2] ] = true;
      }
      // TODO: FILL ONLY WHEN ALL SPACES ARE EMPTY!!!
      //console.log("TODO TODO TODO!!!");
      console.log(":) Space available :)");
      return true;
    } else {
      console.log("some places are taken!");
      return false;
    }
    //console.log("maxSpace:");
    //console.log(maxSpace);
    return true;
  } else {
    console.log("- brick outside legal space, returning false");
    //console.log("maxSpace:");
    //console.log(maxSpace);
    return false;
  }
}

function spacePosInRange(spacePos) {
  return inSpaceRange(spacePos[0]) && inSpaceRange(spacePos[1]) && inSpaceRange(spacePos[2]);
}

function inSpaceRange(index) {
  return index>=0 && index<= (SETTINGS.MAX_SPACE_MAX - SETTINGS.MAX_SPACE_MIN);
}

function unitPosToSpacePos(pos) {
  var xIndex = pos[0] - SETTINGS.MAX_SPACE_MIN;
  var yIndex = pos[1] - SETTINGS.MAX_SPACE_MIN;
  var zIndex = pos[2] - SETTINGS.MAX_SPACE_MIN;
  return [ xIndex, yIndex, zIndex ];
}

function brickHasSpace(brick) {
  return random(1.0)<0.1;
}

function clonePos(p) {
  return [ p[0], p[1], p[2] ];
}
