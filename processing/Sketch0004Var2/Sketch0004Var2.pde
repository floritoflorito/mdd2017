// a falling bouncing boxs

float boxPositionFloat; // define a variable like 3.1415
float speed = 0.5;
float acceleration = 1.5;

void setup() {
  size(333,777);
}

void draw() {
  background(0);
  rect(150,boxPositionFloat,100,100);
  boxPositionFloat = boxPositionFloat + speed;
  speed = speed + acceleration;
  
  // bounce when we reach the bottom screen
  if (boxPositionFloat>999-100) {
    boxPositionFloat = 999-100;
    speed = speed * -0.9;
    fill(random(255),random(255),random(255));
  }
  
  
}
