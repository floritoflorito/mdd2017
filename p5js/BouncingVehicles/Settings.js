function Settings() {
  
  this.BALL_AMOUNT = Math.floor(5+Math.random()*10);
  this.BALL_SPEED = 5;
  this.BALL_BLACK_CHANCE = 0.4;
  
  this.BOUNCE_DRAG = 0.95; //0.8
  this.GRAVITY = 0.5; //0.05
  
  this.ROLL_MOTION_THRESHOLD = 0.05
  this.ROLL_MOTION_DRAG = 0.98;
  
  this.STOP_MOTION_THRESHOLD_SQ = (0.05*0.05);
  
  this.PIXEL_AMOUNT = 3000;
  this.PIXEL_SPEED_FROM_BALL_SPEED_FACTOR = 0.05; //0.3
  this.PIXEL_DIRECTION_SMOOTH = 0.051;
  this.PIXEL_ALPHA = 16;
}

var SETTINGS = new Settings();
