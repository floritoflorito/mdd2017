function SoundSensor(radius) {

  var enableSound = false;
  var x, y;
  var overHole = false;
  var sound = new p5.Noise("pink");
  sound.amp(0.1);
  
  if (!enableSound) console.log("SoundSensor sound disabled!");

  this.setPosition = function(px, py) {
    x=px;
    y=py;
  }

  this.getPosition = function() {
    return { 
    x:
    x, y:
      y
    };
  }

  this.getRadius = function() { 
    return radius;
  }
  this.setOverHole = function(oh) { 
    overHole = oh;

    if (enableSound) {
      if (overHole) {
        sound.start();
      } else {
        sound.stop();
      }
    }
  }

  this.draw = function() {
    noFill();
    stroke(255, 0, 0);
    strokeWeight(overHole ? 5 : 1);
    ellipseMode(RADIUS);
    ellipse(x, y, radius, radius);
    strokeWeight(1);
  }
}
