function Point(x,y,variance) {
  this.x=x;
  this.y=y;
  this.variance = variance;
  this.toString = function() { return "Point("+x+","+y+")"; }
}
