// an animated box moving from left to right

int boxPosition = 100; // define an integer like 1, 2, 3 or so
float boxPositionFloat; // define a variable like 3.1415

void setup() {
  size(999,222);
  boxPosition = 0;
  boxPositionFloat = 0;
}

void draw() {
  background(0);
  rect(boxPositionFloat,20,100,100);
  boxPositionFloat = boxPositionFloat + 3.5;
}
