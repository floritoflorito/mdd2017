function Settings() {
  
  this.BRICK_BASE_SIZE = 30;
  this.BRICK_LENGTH_UNITS_MIN = 1;
  this.BRICK_LENGTH_UNITS_MAX = 8;
  
  this.GROW_SHRINK_SPEED_ONE_DIRECTION = 5.0; // units / second
  this.HOLD_PHASE_LENGTH = 3.0;
  
  this.CHILD_SPAWN_CHANCE_PER_FRAME = 0.5;//0.04;
  
  // important to keep 0, 1, 2 (xyz)
  this.ORIENTATION_X = 0;
  this.ORIENTATION_Y = 1;
  this.ORIENTATION_Z = 2;
  this.ORIENTATION_NAMES = [ "x-axis", "y-axis", "z-axis" ];
  
}
