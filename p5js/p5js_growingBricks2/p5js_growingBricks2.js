var SETTINGS = null;
var bricks = new Array();
var total = 0;
var maxSpace = [];

function setup() {
  createCanvas(400, 400, WEBGL);
  SETTINGS = new Settings();
  initMaxSpace();
  brick = new Brick(0, 0, 0, makeRandomBrickLength(), floor(random(3)));
  bricks.push(brick);
  //console.log(brick);
  //bricks.push( new Brick(0,0,0,makeRandomBrickLength(), floor(random(3)) ) );
}

function draw() {
  background(240);

  //var camX = map(sin(map(frameCount,0,100,0,TWO_PI)),-1,1,-400,400);
  camera(0, 0, 600, 0, 0, 0, 0, 1, 0);

  //lights();

  rotateX(radians(15));
  rotateY(radians(frameCount/2));

  // CHILD SPAWNING
  //if (total<10) {
  for (var i=0; i<bricks.length; i++) {
    var brick = bricks[i];
    var child = brick.childSpawning();
    if (child!=null) {
      //total++;
      bricks.push(child);
    }
  }
  //}


  // SIZE UPDATE & REMOVAL
  for (var i=0; i<bricks.length; i++) {
    var brick = bricks[i];
    brick.sizeUpdate();
    if (brick.isDying()) {
      removeBrickFromSpace(brick);
      bricks.splice(i, 1);
      i--;
    }
  }



  // DRAWING
  for (var i=0; i<bricks.length; i++) {
    var brick = bricks[i];
    brick.draw();
  }
}
