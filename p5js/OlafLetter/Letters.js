function Letters() {

  this.A = new Letter( 0.5, 
    [
    new Line(new Point(0, 1), new Point(0.5, 0)), 
    new Line(new Point(0.5, 0), new Point(1, 1)), 
    new Line(new Point(0.25, 0.5), new Point(0.75, 0.5)), 
    ]
    );

  this.B = new Letter( 0.5, [
    new Line(new Point(0, 0), new Point(0, 1)), 
    new FreeShape( -1, [
    new Point (-1, 0.125, "point"), 
    new Point (0, 0, "point"), 
    new Point (1, 0.25, "point"), 
    new Point (0, 0.5, "point"), 
    new Point (-1, 0.375, "point")
    ])
    , 
    new FreeShape( -1, [
    new Point (-1, 0.625, "point"), 
    new Point (0, 0.5, "point"), 
    new Point (1, 0.75, "point"), 
    new Point (0, 1, "point"), 
    new Point (-1, 0.875, "point")
    ])
    ]);

  //this.C = new Letter( 0.5, [
  //  new FreeShape( -1, [
  //   new Point (2,0.25, "point"),
  //   new Point (1,0, "point"),
  //   new Point (0,0.5, "point"),
  //   new Point (1,1, "point"),
  //   new Point (2,0.75, "point")
  //  ])
  //]);
  this.C = new Letter( 0.75, [
    new FreeShape( -1, [
    new Point (2, 1.5, "point"), 
    new Point (1, 0.2, "point"), 
    new Point (0, 0.5, "point"), 
    new Point (1, 0.8, "point"), 
    new Point (2, -0.5, "point")
    ])
    ] );

  //this.D = new Letter( 0.5, [
  //  new Line(new Point(0,0), new Point(0,1)),
  //  new FreeShape( -1, [
  //   new Point (-1,0.25),
  //   new Point (0,0),
  //   new Point (1,0.5),
  //   new Point (0,1),
  //   new Point (-1,0.75)
  //  ])
  //]);
  this.D = new Letter( 0.5, [
    new Line(new Point(0, 0.1), new Point(0, 0.9)), 
    new FreeShape( -1, [
    new Point (-1, 0.75), 
    new Point (0, 0.1), 
    new Point (1, 0.5), 
    new Point (0, 0.9), 
    new Point (-1, 0.25)
    ])
    ]);

  this.E = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(1, 0)), 
    new Line(new Point(0, 0.5), new Point(0.8, 0.5)), 
    new Line(new Point(0, 1), new Point(1, 1)), 
    new Line(new Point(0, 0), new Point(0, 1))
    ]);

  this.F = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(1, 0)), 
    new Line(new Point(0, 0.5), new Point(0.8, 0.5)), 
    new Line(new Point(0, 0), new Point(0, 1))
    ]);

  this.G = new Letter( 0.75, [
    new FreeShape( -1, [
    new Point (2, 1.5, "point"), 
    new Point (1, 0.2, "point"), 
    new Point (0, 0.5, "point"), 
    new Point (1, 0.8, "point"), 
    new Point (2, -0.5, "point")
    ]), 
    new Line( new Point(1, 0.8), new Point(1, 0.5) ), 
    new Line( new Point(1, 0.5), new Point(0.5, 0.5) ), 
    ] );

  this.H = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(0, 1)), 
    new Line(new Point(0, 0.5), new Point(1, 0.5)), 
    new Line(new Point(1, 0), new Point(1, 1))
    ]);

  this.I = new Letter( 0.25, 
    [
    new Line(new Point(0.5, 0), new Point(0.5, 1))
    ]
    );

  this.J = new Letter( 0.5, 
    [
    //new Line(new Point(1,0), new Point(1,0.75)),
    new FreeShape( 0, [
    new Point(1, -2, "point"), 
    new Point(1, 0, "point"), 
    new Point(1, 0.75, "point"), 
    new Point(0.5, 1, "point"), 
    new Point(0, 0.75, "point"), 
    new Point(0, 0.25, "point")
    ])
    ]
    );

  this.K = new Letter ( 0.5, [ 
    new Line(new Point(0, 0), new Point(0, 1)), 
    new Line(new Point(0, 0.5), new Point(1, 0)), 
    new Line(new Point(0, 0.5), new Point(1, 1))
    ]);

  this.L = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(0, 1)), 
    new Line(new Point(0, 1), new Point(1, 1))
    ]);

  this.M = new Letter( 0.75, 
    [
    new Line(new Point(0, 1), new Point(0.25, 0)), 
    new Line(new Point(0.25, 0), new Point(0.5, 0.8)), 
    new Line(new Point(0.5, 0.8), new Point(0.75, 0)), 
    new Line(new Point(0.75, 0), new Point(1, 1)), 
    ]
    );

  this.N = new Letter( 0.5, 
    [
    new Line(new Point(0, 1), new Point(0, 0)), 
    new Line(new Point(0, 0), new Point(1, 1)), 
    new Line(new Point(1, 1), new Point(1, 0)), 
    ]
    );

  this.O = new Letter( 0.75, [
    new FreeShape( -1, [
    new Point(0.5, 1, "point"), 
    new Point(0, 0.6, "point"), 
    new Point(0.5, 0, "point"), 
    new Point(1, 0.5, "point"), 
    new Point(0.5, 1, "point"), 
    new Point(0, 0.4, "point"), 
    new Point(0.5, 0, "point")
    ])
    ]);

  this.P = new Letter( 0.5, [
    new Line(new Point(0, 0), new Point(0, 1)), 
    new FreeShape( -1, [
    new Point (-1, 0.125), 
    new Point (0, 0), 
    new Point (1, 0.25), 
    new Point (0, 0.5), 
    new Point (-1, 0.375)
    ])
    ]);

  this.Q = new Letter( 0.75, [
    new FreeShape( -1, [
    new Point(0.5, 1, "point"), 
    new Point(0, 0.6, "point"), 
    new Point(0.5, 0, "point"), 
    new Point(1, 0.5, "point"), 
    new Point(0.5, 1, "point"), 
    new Point(0, 0.4, "point"), 
    new Point(0.5, 0, "point")
    ]), 
    new Line( new Point(0.5, 0.5), new Point(1, 1) )
    ]);

  this.R = new Letter( 0.5, [
    new Line(new Point(0, 0), new Point(0, 1)), 
    new FreeShape( -1, [
    new Point (-1, 0.125), 
    new Point (0, 0), 
    new Point (1, 0.25), 
    new Point (0, 0.5), 
    new Point (-1, 0.375)
    ]), 
    new Line( new Point(0.3, 0.5), new Point(1, 1) )
    ]);

  this.S = new Letter( 0.5, [
    new FreeShape( -1, [
    //new Point (-1, 0.625),
    new Point (2, 1.2), 
    new Point (1, 0.2), 
    new Point (0, 0.3), 
    //new Point (0.5,0.5),
    new Point (1, 0.7), 
    new Point (0, 0.9), 
    new Point (-1, 0.2)
    ])
    ]);

  this.T = new Letter ( 0.5, [
    new Line(new Point(0.5, 0), new Point(0.5, 1)), 
    new Line(new Point(0, 0), new Point(1, 0)), 
    ]);

  this.U = new Letter( 0.5, 
    [
    //new Line(new Point(1,0), new Point(1,0.75)),
    new FreeShape( 0, [
    new Point(1, -2, "point"), 
    new Point(1, 0, "point"), 
    new Point(1, 0.75, "point"), 
    new Point(0.5, 1, "point"), 
    new Point(0, 0.75, "point"), 
    //new Point(0, 0.25, "point"),
    new Point(0, 0, "point"), 
    new Point(0, -2, "point")
    ])
    ]);

  this.V = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(0.5, 1)), 
    new Line(new Point(0.5, 1), new Point(1, 0))
    ]);

  this.W = new Letter( 0.75, 
    [
    new Line(new Point(0, 0), new Point(0.25, 1)), 
    new Line(new Point(0.25, 1), new Point(0.5, 0.2)), 
    new Line(new Point(0.5, 0.2), new Point(0.75, 1)), 
    new Line(new Point(0.75, 1), new Point(1, 0)), 
    ]);

  this.X = new Letter ( 0.5, [
    new Line(new Point(1, 0), new Point(0, 1)), 
    new Line(new Point(0, 0), new Point(1, 1))
    ]);

  this.Y = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(0.5, 0.5)), 
    new Line(new Point(0.5, 0.5), new Point(1, 0)), 
    new Line(new Point(0.5, 0.5), new Point(0.5, 1))
    ]);

  this.Z = new Letter ( 0.5, [
    new Line(new Point(0, 0), new Point(1, 0)), 
    new Line(new Point(1, 0), new Point(0, 1)), 
    new Line(new Point(0, 1), new Point(1, 1))
    ]);

  this.SPACE = new Letter( 0.5, [] );

  this.ALL_LETTERS = [ 
    this.SPACE, this.A, this.B, this.C, this.D, this.E, this.F, this.G, this.H, 
    this.I, this.J, this.K, this.L, this.M, this.N, this.O, this.P, 
    this.Q, this.R, this.S, this.T, this.U, this.V, this.W, this.X, 
    this.Y, this.Z
  ];
  this.NAME = [ this.B, this.A, this.M, this.B, this.I ];
  this.getRandomLetter = function() {
    var index = Math.floor(Math.random()*this.ALL_LETTERS.length);
    return this.ALL_LETTERS[index];
  }

  this.createLetters = function(text) {
    text = text.toUpperCase();
    //console.log(txt);
    var arr = [];
    for (var i=0; i<text.length; i++) {
      var character = text[i];

      var outputSet = false;
      if (character==" ") {
        arr.push( letters.SPACE  );
        outputSet = true;
      } else {
        arr.push( letters[character] );
        outputSet = true;
      }
      if (!outputSet) {
        arr.push(undefined);
      }
    }
    //text.push( letters[txt[i]] );
    return arr;
  }

  //console.log("B: "+this.getShapes());
}
