function Gear(toothWidth, toothHeight, teethCount, outerTeethWidthPercentageDecrease, rotation) {

  if (outerTeethWidthPercentageDecrease==undefined)
    outerTeethWidthPercentageDecrease = 0.1;
  //var outerTeethWidthPercentageDecrease = 0.1;
  //var outerRadianStep = radians(1
  var innerCircumference = teethCount * toothWidth * 2;
  var innerRadius = innerCircumference / (2*PI);
  var outerRadius = innerRadius + toothHeight;
  var outerCircumference = outerRadius * 2 * PI;
  if (rotation==undefined) rotation = 0;

  var rotationPercentageSinceStart = 0;
  var startRotation = rotation;

  var innerCircumferenceSpeed = 0; // pixels per step
  var outerCircumferenceSpeed = 0;
  var percentageSpeed = 0;
  
  var _this = this;
  
  var holes = [];

  var x=0, y=0;
  
  this.setPosition = function(px,py) {
    x = px;
    y = py;
  }
  
  this.getX = function() { return x; }
  this.getY = function() { return y; }

  

  /*
   *
   *  INCREASE ROTATION
   *
   */


  this.increaseRotation = function(deltaRadians) {
    rotation += deltaRadians;
    percentageSpeed = deltaRadians/TWO_PI;
    innerCircumferenceSpeed = innerCircumference * percentageSpeed;// / (2*PI);
    outerCircumferenceSpeed = outerCircumference * percentageSpeed;// / (2*PI);
  }

  this.increaseRotationByOuterCircumferenceSpeed = function (ocs) {
    outerCircumferenceSpeed = ocs;
    innerCircumferenceSpeed = outerCircumferenceSpeed / outerCircumference * innerCircumference;
    var percentageOfCircleStep = outerCircumferenceSpeed / outerCircumference;// * (2*PI);
    var deltaRadians = percentageOfCircleStep * TWO_PI;
    rotation += deltaRadians;
  }

  this.increaseRotationByPercentage = function(p) {
    percentageSpeed = p;
    rotation += percentageSpeed*TWO_PI;
  }

  /*
  *
   *    UPDATE
   *
   */


  this.update = function() {
    var rotationSinceStart = abs(rotation-startRotation);
    rotationPercentageSinceStart = (rotationSinceStart)/TWO_PI;
    //var rotDeg = degrees(rotationSinceStart);
    //console.log(rotDeg);
  }

  /*
  *
  *    DRAW
  *
  */

  this.draw = function() {//x, y) {
    ellipseMode(RADIUS);
    //stroke(128);
    noFill();
    
    //drawHoleGlobal(x,y);

    push();
    translate(x, y);
    stroke(0);
    strokeWeight(1);
    text(teethCount,-5,5);
    
    //ellipse(0,0,innderRadius);
    //ellipse(0,0,outerRadius);

    var radians = 0;
    var radStep = TWO_PI/(2*teethCount);
    var radStepForOuterTooth = 2*radStep * outerTeethWidthPercentageDecrease;
    radStep = radStep - radStepForOuterTooth;
    stroke(0);
    rotate(rotation);
    beginShape();
    for (var i=0; i<teethCount; i++) {
      var tx = innerRadius * cos(radians);
      var ty = innerRadius * sin(radians);
      vertex(tx, ty);
      radians += radStepForOuterTooth;
      tx = outerRadius * cos(radians);
      ty = outerRadius * sin(radians);
      vertex(tx, ty);
      radians += radStep;
      tx = outerRadius * cos(radians);
      ty = outerRadius * sin(radians);
      vertex(tx, ty);
      radians += radStepForOuterTooth;
      tx = innerRadius * cos(radians);
      ty = innerRadius * sin(radians);
      vertex(tx, ty);
      radians += radStep;
    }
    endShape(CLOSE);
    
    for (var i=0;i<holes.length;i++) {
      holes[i].drawLocal();
    }
    
    pop();
  } // end drawAt
  
  
  
  /*
  *
  *    HOLES
  *
  */
  
  
  this.addHole = function(circlePercentage, distanceFromEdge, size) {
    var hole = new GearHole(_this, circlePercentage, distanceFromEdge, size);
    holes.push(hole);
  }
  
  function drawHoleGlobal() {
    var positions = calcHoleGlobalPositions();
    ellipseMode(RADIUS);
    noFill();
    strokeWeight(1);
    for (var i=0;i<positions.length;i++) {
      ellipse(positions[i].x, positions[i].y, 10, 10);
    }
  }
  
  
  this.getHoleGlobalPositions = function() {
    return calcHoleGlobalPositions();
  }
  
  function calcHoleGlobalPositions() {
    var out = [];
    
    for (var i=0;i<holes.length;i++) {
      var hx = x;
      var hy = y;
      var hole = holes[i];
      var r = innerRadius - hole.getDistanceFromEdge();
      hx += cos(rotation+hole.getCirclePercentage()*TWO_PI)*r;
      hy += sin(rotation+hole.getCirclePercentage()*TWO_PI)*r;
      out.push( {x:hx, y:hy} );
    }
    return out;
  }






  this.getAverageRadius = function() { 
    return (innerRadius+outerRadius)/2;
  }
  
  this.getInnerRadius = function() {
    return innerRadius;
  }

  this.getInnerCircumferenceSpeed = function() { 
    return innerCircumferenceSpeed;
  }
  this.getOuterCircumferenceSpeed = function() { 
    return outerCircumferenceSpeed;
  }
  this.getAverageCircumferenceSpeed = function() { 
    return (innerCircumferenceSpeed+outerCircumferenceSpeed)/2;
  }
  this.getRotationPercentageSinceStart = function() { 
    return rotationPercentageSinceStart;
  }
  this.getRotationPercentageSpeed = function() { 
    return percentageSpeed;
  }
}
