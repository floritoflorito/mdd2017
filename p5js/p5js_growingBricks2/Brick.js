function Brick(_x,_y,_z,_singleDirectionLength,_orientation) {
  //console.log("Brick()");
  
  var PHASE_GROWING = 0;
  var PHASE_HOLDING = 1;
  var PHASE_SHRINKING = 2;
  var PHASE_DYING = 3;
  
  var _phase = PHASE_GROWING; // growing
  var _length = 0;//
  var _startTime = millis();
  
  var endTimeGrow = _singleDirectionLength / SETTINGS.GROW_SHRINK_SPEED_ONE_DIRECTION;
  var endTimeHold = endTimeGrow + SETTINGS.HOLD_PHASE_LENGTH;
  var endTimeShrink = endTimeHold + endTimeGrow;
  //console.log("phases: 0.0 ... "+endTimeGrow+" ... "+endTimeHold+" ... "+endTimeShrink);
  
  var childrenCount = 0;
  
  var _this = this;
  //console.log("thisThing="+_this);
  //console.log(_this);
  
  this.addedToSpace = false;
  
  
  /*
  *  SIZE UPDATE
  */
  
  this.sizeUpdate = function() {
    
    var age = (millis()-_startTime)/1000;
    //console.log("age = "+age);
    
    //_singleDirectionLength=0;
    //[x] (l=1)
    //_singleDirectionLength=1
    //[.][x][.] (l=3)
    //_singleDirectionLength=2
    //[.][.][x][.][.] (l=5)
    
    var totalMaxLength = 1 + 2*(_singleDirectionLength);
    
    if (age<endTimeGrow) {
      _phase = PHASE_GROWING;
      var radians = map(age,0,endTimeGrow,0,HALF_PI);
      _length = map(sin(radians),0,1,0,totalMaxLength);
    } else if (age<endTimeHold) {
      _phase = PHASE_HOLDING;
      _length = totalMaxLength;
    } else if (age<endTimeShrink) {
      _phase = PHASE_SHRINKING;
      var radians = map(age,endTimeHold,endTimeShrink,HALF_PI,0);
      _length = map(sin(radians),1,0,totalMaxLength,0);
    } else {
      _phase = PHASE_DYING;
    }
    
  }
  
  
  
  
  /*
  *  CHILD SPAWNING
  */
  
  this.childSpawning = function() {
    
    if (_phase == PHASE_HOLDING) {
      
      if (random(1.0) < SETTINGS.CHILD_SPAWN_CHANCE_PER_FRAME) {
        var tryCount = 0;
        var child = null;
        while (child==null && tryCount<SETTINGS.CHILD_SPAWN_TRIES_PER_FRAME) {
          var child = spawnBrickChild(_this);
          tryCount++;
        }
        return child;
      }
      
    }
    
    return null;
  }
  
  
  
  
  
  
  /*
  *  DRAWING
  */  
  
  this.draw = function() {
    //console.log("Brick.draw(), _phase="+_phase);
    //console.log(_phase+" "+(_phase == PHASE_DYING));
    //console.log(this.isDying());
    if (!this.isDying()) {
      push();
      //stroke(0,0,0);
      //fill(128,64,64);
      translate(_x*SETTINGS.BRICK_BASE_SIZE,_y*SETTINGS.BRICK_BASE_SIZE,_z*SETTINGS.BRICK_BASE_SIZE);
      //scale(SETTINGS.BRICK_BASE_SIZE);
      if (_orientation==SETTINGS.ORIENTATION_X) {
        box(_length*SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE);
      } else if (_orientation==SETTINGS.ORIENTATION_Y) {
        box(SETTINGS.BRICK_BASE_SIZE, _length*SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE);
      } else if (_orientation==SETTINGS.ORIENTATION_Z) {
        box(SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE, _length*SETTINGS.BRICK_BASE_SIZE);
      }
      //box(_w,_h,_d);
      pop();
    }
  }
  
  this.getPosition = function() {
    return [ _x, _y, _z ];
  }
  
  this.getOrientation = function() {
    return _orientation;
  }
  
  this.getSingleDirectionLength = function() {
    return _singleDirectionLength;
  }
  
  this.isDying = function() {
    return _phase == PHASE_DYING;
  }
  
  this.toString = function() {
    console.log("Brick[ singleLength="+_singleDirectionLength+"]");
  }
  
  
  this.addedToSpace = addBrickToSpace(_this);
  //console.log("Brick creation, added to space? "+this.addedToSpace);
}

/*
*  BRICK TOOLS
*/

function makeRandomBrickLength() {
  var dualUnits = floor(random(SETTINGS.BRICK_LENGTH_DUAL_EXTENSION_MIN,SETTINGS.BRICK_LENGTH_DUAL_EXTENSION_MAX));
  return dualUnits;
}
