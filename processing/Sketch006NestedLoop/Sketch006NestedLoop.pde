// nested loops: making noise on screen

void setup() {
  size(400, 400);
}

void draw() {

  for (int y=0; y<height; y=y+1) {
    for (int x=0; x<width; x=x+1) {
      //stroke(random(255), random(255), random(255));
      //point(x, y);
      set(x,y,color(random(255),random(255),random(255)));
    }
  }
}
