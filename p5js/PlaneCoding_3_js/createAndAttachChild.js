
function createAndAttachChild() {
  var parent = getRandomFucker();
  var child = generateAndAttachChild(parent);
  parent.children.push(child);
  allFuckers.push(child);
}

function getRandomFucker() {
  var index;
  do {
    index = floor(random(allFuckers.length));
  } while (allFuckers[index].treeLevel>=SETTINGS.MAX_TREE_LEVELS);

  return allFuckers[index];
}

function generateAndAttachChild(parent) {
  var debug = false;
  if (debug) console.log("------------------------");
  if (debug) console.log("generateAndAttachChild()");
  
  if (debug) console.log("- parent="+parent);
  var tx = randomPM() * random(parent.dims[0]) * SETTINGS.FRAGMENTATION;
  var ty = randomPM() * random(parent.dims[1]) * SETTINGS.FRAGMENTATION;
  var tz = randomPM() * random(parent.dims[2]) * SETTINGS.FRAGMENTATION;
  if (debug) console.log("- translate "+tx+", "+ty+", "+tz);
  
  var type = parent.type; //TODO: other types
  if (debug) console.log("- type="+type);
  
  var dims = [
    parent.dims[0] * random(SETTINGS.CHILD_SIZE_MIN, SETTINGS.CHILD_SIZE_MAX), 
    parent.dims[1] * random(SETTINGS.CHILD_SIZE_MIN, SETTINGS.CHILD_SIZE_MAX), 
    parent.dims[2] * random(SETTINGS.CHILD_SIZE_MIN, SETTINGS.CHILD_SIZE_MAX)
  ];
  if (debug) console.log("= dims="+dims);
  var colorVariation = random(SETTINGS.MIN_COLOR_MORPH,SETTINGS.MAX_COLOR_MORPH);
  var myColor = variateColor(parent.myColor, colorVariation);

  var child = new Fucker(tx, ty, tz, type, dims, myColor, parent.treeLevel+1, 
    parent.rotOscilFreq, parent.rotOscilPhase + SETTINGS.ROT_OSCIL_CHILD_PHASE_OFFSET);
  child.parent = parent;
  child.detached = false;
  
  if (debug) console.log("Generated child:");
  if (debug) console.log(child);
  
  return child;
}
