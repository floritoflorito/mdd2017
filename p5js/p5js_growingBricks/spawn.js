function spawnBrickChild(brickParent) {

  console.log("spawnBrickChild()");
  //console.log(brickParent);
  //console.log(brickParent.getLength());
  
  var parentUnitLength = getBrickLengthInUnits(brickParent);
  var parentAxisUnitOffset = floor(random(0,parentUnitLength)); // offset along the same axis of parent (in units)
  
  var xyzOffsetUnits = [ 0,0,0 ];
  var parentOrientation = brickParent.getOrientation();
  xyzOffsetUnits[parentOrientation] = parentAxisUnitOffset;
  
  var otherAxis = [];
  for (var i=0;i<3;i++) {
    if (i!=parentOrientation) otherAxis.push(i);
  }
  
  //for (var i=0;i<otherAxis.length;i++) {
  //  var axis = otherAxis[i];
  //  xyzOffsetUnits[axis] = random(1)<0.5 ? -1 : 1;
  //}
  var orientation = floor(random(3));
  var singleOtherAxis;
  if (otherAxis[0] == orientation) singleOtherAxis = otherAxis[1];
  else if (otherAxis[1] == orientation) singleOtherAxis = otherAxis[0];
  else singleOtherAxis = otherAxis[ floor(random(2)) ];
  
  xyzOffsetUnits[singleOtherAxis] = random(1)<0.5 ? -1 : 1;
  
  console.log("create child from parent orientation "+SETTINGS.ORIENTATION_NAMES[parentOrientation]);
  console.log(xyzOffsetUnits);
  
  //var childOrientation = floor(random(3));
  
  var x = SETTINGS.BRICK_BASE_SIZE * xyzOffsetUnits[0];
  var y = SETTINGS.BRICK_BASE_SIZE * xyzOffsetUnits[1];
  var z = SETTINGS.BRICK_BASE_SIZE * xyzOffsetUnits[2];
  var brickLength = makeRandomBrickLength();
  
  var child = new Brick(x,y,z,brickLength,orientation);
  
  

  return child;
}
