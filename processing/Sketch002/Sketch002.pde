// slightly randomly coloured concentric circles centered around the mouse

void setup() {
  //size(400,300);
  fullScreen();
  background(0);
}

void draw() {
  //background(0);
  fill(255,0,0);
  stroke(0,random(255),0);
  strokeWeight(10);
  //noStroke();
  //noFill();
  ellipse(mouseX,mouseY,50,50);
}
