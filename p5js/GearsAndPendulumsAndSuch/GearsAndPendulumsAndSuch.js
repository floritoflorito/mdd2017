var gear0; //24
var gear1; //8
var gear2; //16
var allGears;
var rotating = true;

var sensor0;
var sensor1;
var sensor2;
var allSensors;

var gear0Rotation;

var p1 = [1,0,0,1,0,0,1,0];
var p3 = [1,0,0,1,0,1,0];

console.log("press [space], [,], [.]");

function keyTyped() {
  if (key==' ') rotating = ! rotating;
  else if (key==',') gear0Rotation *= 0.9;
  else if (key=='.') gear0Rotation /= 0.9;
  console.log(gear0Rotation);
}

function setup() {
  createCanvas(800, 400);
  gear0Rotation = radians(0.5);
  gear0 = new Gear(10, 10, 24, 0.1);
  gear0.addHole(1/96,0,5); //circlePercentage, distanceFromEdge, weight
  gear0.addHole(1/96+2/24,0,5);
  gear0.addHole(1/96-5/24,0,5);
  //gear0.addHole(1/12,0,5);
  //gear0.addHole(2/12,0,5);
  gear1 = new Gear(10, 10, 8, 0.2, radians(-16));
  for (var i=0;i<8;i++) 
    if (p1[i]==1)
      gear1.addHole(0.5+1/(3*8)+i/8,0,5); //circlePercentage, distanceFromEdge, size
  gear2 = new Gear(10, 10, 16, 0.15, radians(9));
  gear3 = new Gear(10, 10, 7, 0.2, radians(-7)); 
  for (var i=0;i<p3.length;i++) if (p3[i]==1) gear3.addHole(1/(3*7)-i/7,0,5);
  
  gear0.setPosition(200,200);
  gear1.setPosition(gear0.getX()+gear0.getAverageRadius()+gear1.getAverageRadius()+2, gear0.getY()-5);
  gear2.setPosition(331,282);
  gear3.setPosition(370,175);
  allGears = [ gear0, gear1, gear2, gear3 ];
  
  sensor0 = new SoundSensor(5);
  sensor0.setPosition( gear1.getHoleGlobalPositions()[0].x, gear1.getHoleGlobalPositions()[0].y);//310,170);
  sensor1 = new SoundSensor(5);
  sensor1.setPosition( gear0.getHoleGlobalPositions()[0].x, gear0.getHoleGlobalPositions()[0].y );
  sensor2 = new SoundSensor(5);
  sensor2.setPosition( gear3.getHoleGlobalPositions()[0].x, gear3.getHoleGlobalPositions()[0].y );
  allSensors = [ sensor0, sensor1, sensor2 ];
  
}

function draw() {
  background(240);
  //push();
  //translate(200, 200);

  gear0.draw();//At(0, 0);
  gear1.draw();//At(gear0.getAverageRadius()+gear1.getAverageRadius()+2, -5);
  gear2.draw();//At(gear0.getAverageRadius()+gear1.getAverageRadius()*2+gear2.getAverageRadius()+4, 0);
  gear3.draw();
  sensor0.draw(); 
  sensor1.draw();
  sensor2.draw();

  if (rotating) {
    gear0.increaseRotation(gear0Rotation);
    var spd = gear0.getRotationPercentageSpeed();//getInnerCircumferenceSpeed();
    gear1.increaseRotationByPercentage( -spd*3 );
    gear2.increaseRotationByPercentage( gear1.getRotationPercentageSpeed()/-2 );
    gear3.increaseRotationByPercentage( gear1.getRotationPercentageSpeed() * -8/7 );
  }

  gear0.update();
  gear1.update();
  gear2.update();
  gear3.update();
  
  // get all hole positions
  var allHolePositions = [];
  for (var i=0;i<allGears.length;i++) {
    var positions = allGears[i].getHoleGlobalPositions();
    for (var j=0;j<positions.length;j++) allHolePositions.push(positions[j]);
  }
  
  for (var i=0;i<allSensors.length;i++) {
    var sensor = allSensors[i];
    var sensorPos = sensor.getPosition();
    var sensorRadius = sensor.getRadius();
    var sx = sensorPos.x;
    var sy = sensorPos.y;
    //sensor.setOverHole(false);
    var overOneHole = false;
    for (var h=0;h<allHolePositions.length;h++) {
      var holePos = allHolePositions[h];
      var hx = holePos.x;
      var hy = holePos.y;
      var dx = hx-sx;
      var dy = hy-sy;
      var d = sqrt(dx*dx+dy*dy);
      //console.log(h+": sensor at "+sx+"/"+sy+", hole at "+round(hx)+"/"+round(hy)+" d="+d+" ("+sensorRadius+")");
      if (d<sensorRadius) {
        overOneHole = true;//sensor.setOverHole(true); 
      }
    }
    sensor.setOverHole( overOneHole );
  } // end for all sensors
  
  //console.log(allHolePositions);
  
  //pop();
  
  

  
  //var g0 = gear0.getRotationPercentageSinceStart();
  //var g1 = gear1.getRotationPercentageSinceStart(); 
  //text("Gear0 percentage: "+nf(g0*100,1,1)+"%",10,20);
  //text("Gear1 percentage: "+nf(g1*100,1,1)+"%",10,35);
}
