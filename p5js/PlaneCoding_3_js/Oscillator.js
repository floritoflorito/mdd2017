

function Oscillator(_frequency,_phase, _type,_typeParam) {
  
  var type;
  var typeParam;
  var frequency = 1;
  var phase = 0;
  var lastMs;
  var amp;
  
  //console.log("new Oscillator with f="+_frequency+", p="+_phase+", t="+_type+", params="+_typeParam);
  
  frequency = max(0.0000000001, _frequency);
  phase = _phase;
  type = _type;
  typeParam = _typeParam;
  lastMs = millis();
  
  
  this.update = function() {
    var ms = millis();
    var deltaSeconds = (ms-lastMs)/1000.0;
   
    
    var deltaPhase = deltaSeconds * frequency;
    
    phase += deltaPhase;
    phase %= 1.0;
    
    switch (type) {
      case SETTINGS.OSCILLATOR_SINEXP:
      amp = sin01exp(phase*TWO_PI, typeParam);
      
      break;
    }
    
    lastMs = ms;
    
    
    return amp;
  }
  
}
