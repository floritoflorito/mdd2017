// characterHeight in pixels
// pointVariation in percentage

function LetterRenderer(props) {//characterHeight, characterSpacing, pointVariation, curveVariation) {

  this.debugArea = false;
  var debugPoints = false;
  
  var characterHeight = props[CHARACTER_HEIGHT];
  var characterSpacing = props[CHARACTER_SPACE];
  var pointVariation = props[POINT_VARIATION];
  var curveVariation = props[CURVE_VARIATION];
  var strokeColor = props[STROKE_COLOR]==undefined ? color(0) : props[STROKE_COLOR];
  var theStrokeWeight = props[STROKE_WEIGHT];
  var randomSeedInit = props[RANDOM_SEED_INIT];
  var randomSeedRender = props[RANDOM_SEED_RENDER];
  
  
  
  var xPos = 0;
  var yPos = 0;
  
  this.init = function() {
    resetPosition();
    if (randomSeedInit!=undefined)
      randomSeed(randomSeedInit);
  }
  this.init();
  
  this.setPointVariation = function(pv) {
    pointVariation = pv;
  }
  
  this.setCurveVariation = function(cv) {
    curveVariation = cv;
  }

  this.render = function(letter) {
    if (letter==undefined) return;
    
    var aspRat = letter.getAspectRatio();
    var characterWidth = characterHeight * aspRat;

    var maxPointVariation = characterHeight * pointVariation;
    var maxCurveVariation = characterHeight * curveVariation;

    if (this.debugArea) {
      stroke(0, 32);
      rect(xPos, yPos, characterWidth, characterHeight);
      stroke(0);
    }
    
    strokeWeight(theStrokeWeight);
    
    //stroke(random(255), random(255), random(255));

    if (randomSeedRender!=undefined) {
      randomSeed(randomSeedRender);
    }

    stroke(strokeColor);
    var shapes = letter.getShapes();
    //console.log("shapes.length="+shapes.length);
    var count = shapes.length;
    for (var shapeIndex=0; shapeIndex<count; shapeIndex++) {
      //console.log("Render shape "+i+": "+shapes[i].type);
      var shape = shapes[shapeIndex];

      switch (shape.type) {
      case SHAPE_TYPE_LINE:
        var vertex0 = new Point(
          xPos + shape.startPoint.x * characterWidth  + random(-maxCurveVariation, maxCurveVariation), 
          yPos + shape.startPoint.y * characterHeight + random(-maxCurveVariation, maxCurveVariation)
          );
        var vertex1 = new Point(
          xPos + shape.startPoint.x * characterWidth  + random(-maxPointVariation, maxPointVariation), 
          yPos + shape.startPoint.y * characterHeight + random(-maxPointVariation, maxPointVariation)
          );
        var vertex2 = new Point(
          xPos + shape.endPoint.x * characterWidth  + random(-maxPointVariation, maxPointVariation), 
          yPos + shape.endPoint.y * characterHeight + random(-maxPointVariation, maxPointVariation)
          );
        var vertex3 = new Point(
          xPos + shape.endPoint.x * characterWidth  + random(-maxCurveVariation, maxCurveVariation), 
          yPos + shape.endPoint.y * characterHeight + random(-maxCurveVariation, maxCurveVariation)
          );

        curveTightness(0);
        beginShape();
        cVertex(vertex0);
        cVertex(vertex1);
        cVertex(vertex2);
        cVertex(vertex3);
        endShape();
        
        if (debugPoints) {
          strokeWeight(4);
          dPoint(vertex0);
          dPoint(vertex1);
          dPoint(vertex2);
          dPoint(vertex3);
          strokeWeight(1);
        }
        break;



      case SHAPE_TYPE_FREE_SHAPE:
        var lastIndex = shape.points.length-1;
        var vertices = [];
        for (var i=0; i<shape.points.length; i++) {
          var maxVariation = maxPointVariation;
          if (shape.points[i].variance=="point") {
            maxVariation = maxPointVariation;
          } else if (shape.points[i].variance=="curve") {
            maxVariation = maxCurveVariation;
          }
          vertices.push(
            new Point(
            xPos + shape.points[i].x * characterWidth + random(-maxVariation, maxVariation), 
            yPos + shape.points[i].y * characterHeight + random(-maxVariation, maxVariation)
            )
            );
        }

        curveTightness(shape.tightness);
        beginShape();
        for (var i=0; i<shape.points.length; i++)
          cVertex(vertices[i]);
        endShape();

        if (debugPoints) {
          strokeWeight(4);
          for (var i=0; i<shape.points.length; i++)
            dPoint(vertices[i]);
          strokeWeight(1);
        }
        
        break;
        
        
      } // end switch shape type
      //var t
    }

    xPos += characterWidth + characterSpacing;
    
  } // end this.render


  function resetPosition() {
    xPos = 0;
    yPos = 0;
  }
  
  this.resetPosition = function() {
    resetPosition();
  }
  
  
  this.nextRow = function() {
    yPos += characterSpacing + characterHeight;
    xPos = 0;
  }
  
  this.getXPos = function() {
    return xPos;
  }
  this.getYPos = function() {
    return yPos;
  }
  
  this.setRandomSeedInit = function(s) {
    randomSeedInit = s;
  }
  
  this.setRandomSeedRender = function(s) {
    randomSeedRender = s;
  }
  
  this.setXPos = function(xp) { xPos = xp; };
  this.setYPos = function(yp) { yPos = yp; };
  
  this.getPointVariation = function() { return pointVariation; }
  this.setPointVariation = function(v) { pointVariation = v; }
  this.getCurveVariation = function() { return curveVariation; }
  this.setCurveVariation = function(v) { curveVariation = v; }
}



function cVertex(point) {
  curveVertex(point.x, point.y);
}

function dPoint(pnt) {
  point(pnt.x, pnt.y);
}
