var balls;
var dots;

var showBalls = true;
var trace = true;

function setup() {
  createCanvas(800,400);
  background(0);
  
  balls = [];
  for (var i=0;i<SETTINGS.BALL_AMOUNT;i++) {
    var ball = new Ball(random(width),random(height));
    balls.push(ball);
  }
  
  dots = [];
  for (var i=0;i<SETTINGS.PIXEL_AMOUNT;i++) {
    var ball = balls[ floor(random(balls.length)) ];
    var dot = new Dot(random(width),random(height),balls);
    dots.push(dot);
  }
}

function keyTyped() {
  if (key=='b') showBalls = !showBalls;
}

function draw() {
  if (!trace) background(240);
  else {
  }
  
  for (var i=0;i<SETTINGS.BALL_AMOUNT;i++) {
    var ball = balls[i];
    ball.update();
    if (!trace && showBalls)
      ball.drawBall();
  }
  
  for (var i=0;i<SETTINGS.PIXEL_AMOUNT;i++) {
    var dot = dots[i];
    dot.update();
    dot.draw();
  }
}
