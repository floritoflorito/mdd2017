function FallingBall(position) {

  var startPos = position;
  var velocity = 0;
  var acceleration = 9.81; // earth in m/sˆ2
  var lastTime;
  var startTime;
  var timeSinceStart;
  var done = false;

  this.start = function() {
    position = startPos;
    done = false;
    startTime = millis();
    lastTime = millis();
  }

  this.update = function() {
    if (!done) {
      var now = millis();
      var dt = (now - lastTime)/1000;
      timeSinceStart = (now-startTime)/1000;
      
      lastTime = now;

      velocity = velocity + acceleration*dt;
      position = position - velocity*dt;
      /*
      var newPos = position - velocity*dt - 0.5*acceleration*dt*dt;
       var deltaPos = newPos - position;
       var newVelocity = -deltaPos/dt;
       
       position = newPos;
       velocity = newVelocity;
       
       */

      //console.log("position: "+position+", velocity+"+velocity);

      if (position<0) {
        done = true;
      }
    }
  }


  this.draw = function(factor) {
    var dPos = height-20-position*factor;
    var sPos = height-20-startPos*factor;
    strokeWeight(1);
    line(width/4, height-20, width/4, sPos);
    strokeWeight(5);
    point(width/4, dPos);

    if (!done) {
      text("time since start: "+nf(timeSinceStart,1,2)+"s",10,20);
    } else {
      text("time since start: "+nf(timeSinceStart,1,2)+"s",10,20);
    }
    text(nf(startPos,1,2)+"m",width/4-50,sPos+5);
    text(nf(position, 1, 2)+"m ("+nf(velocity,1,2)+" m/s = "+nf(velocity*3600/1000,1,2)+" km/h)", width/4+10, dPos+5);
  }

  this.getPosition = function() {
    return position;
  }

  this.getStartPosition = function() {
    return startPos;
  }
}
