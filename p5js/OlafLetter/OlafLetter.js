var letters = new Letters();

var demo;

function setup() {
  createCanvas(800, 400);
  demo = new TypingDemo();//AlphabetDemo();
  demo.init();
}

function keyTyped() {
  if (demo.hasOwnProperty("keyTyped"))
    demo.keyTyped();
}

function mousePressed() {
  if (demo.hasOwnProperty("mousePressed"))
    demo.mousePressed();
}

function draw() {
  demo.update();
}
