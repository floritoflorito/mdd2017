// arrays: lots of brownian moving dots

float[] x;
float[] y;

void setup() {
  size(1200,800);
  background(0);
  x = new float[50];
  y = new float[50];
  for (int i=0;i<50;i=i+1) {
    x[i] = width/2;
    y[i] = random(height);
  }
}

void draw() {
  background(128,0,0);
  noStroke();
  fill(#F06FE3,255);
  for (int i=0;i<50;i=i+1) {
    ellipse(x[i],y[i],10,10);
    x[i] = x[i] + random(-10,10);
    y[i] = y[i] + random(-2,2);
  }
}
