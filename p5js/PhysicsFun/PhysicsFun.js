var ball;
var pendulum, pendulum2;

function setup() {
  createCanvas(400, 400);
  ball = new FallingBall(10);//4.9); // meter;
  ball.start();
  pendulum = new Pendulum(1, -45); // meter;
  pendulum.start();
  pendulum2 = new Pendulum(2, -45); // meter;
  pendulum2.start();
}

function draw() {
  background(240);
  ball.update();
  ball.draw(20);
  pendulum.update();
  pendulum.draw(100, 80);
  pendulum2.update();
  pendulum2.draw(100, width-160);
}
