CHARACTER_HEIGHT = "characterHeight";
CHARACTER_SPACE = "characterSpacing";
POINT_VARIATION = "pointVariation";
CURVE_VARIATION = "curveVariation";
STROKE_COLOR = "strokeColor";
STROKE_WEIGHT = "strokeWeight";
RANDOM_SEED_INIT = "randomSeedInit"; //
RANDOM_SEED_RENDER = "randomSeedRender";


var defaultLetterRenderProperties = {};
defaultLetterRenderProperties[CHARACTER_HEIGHT] = 100;
defaultLetterRenderProperties[CHARACTER_SPACE] = 10;
defaultLetterRenderProperties[POINT_VARIATION] = 0.05;
defaultLetterRenderProperties[CURVE_VARIATION] = 0.4;
defaultLetterRenderProperties[STROKE_COLOR] = undefined;
defaultLetterRenderProperties[STROKE_WEIGHT] = 1;
defaultLetterRenderProperties[RANDOM_SEED_INIT] = undefined;
defaultLetterRenderProperties[RANDOM_SEED_RENDER] = undefined;


function createDefaultLetterRenderProperties() {
  return JSON.parse(JSON.stringify(defaultLetterRenderProperties));
}
