function Brick(_x,_y,_z,_maxLength,_orientation) {
  console.log("Brick()");
  
  var PHASE_GROWING = 0;
  var PHASE_HOLDING = 1;
  var PHASE_SHRINKING = 2;
  var PHASE_DYING = 3;
  
  var _phase = PHASE_GROWING; // growing
  var _length = 0;//_maxLength;
  var _startTime = millis();
  
  var endTimeGrow = (_maxLength/2) / SETTINGS.GROW_SHRINK_SPEED_ONE_DIRECTION;
  var endTimeHold = endTimeGrow + SETTINGS.HOLD_PHASE_LENGTH;
  var endTimeShrink = endTimeHold + endTimeGrow;
  //console.log("phases: 0.0 ... "+endTimeGrow+" ... "+endTimeHold+" ... "+endTimeShrink);
  
  var childrenCount = 0;
  
  var _this = this;
  console.log("thisThing="+_this);
  
  addBrickToSpace(_this);
  
  /*
  *  SIZE UPDATE
  */
  
  this.sizeUpdate = function() {
    
    var age = (millis()-_startTime)/1000;
    //console.log("age = "+age);
    
    if (age<endTimeGrow) {
      _phase = PHASE_GROWING;
      var radians = map(age,0,endTimeGrow,0,HALF_PI);
      _length = map(sin(radians),0,1,0,_maxLength);
    } else if (age<endTimeHold) {
      _phase = PHASE_HOLDING;
      _length = _maxLength
    } else if (age<endTimeShrink) {
      _phase = PHASE_SHRINKING;
      var radians = map(age,endTimeHold,endTimeShrink,HALF_PI,0);
      _length = map(sin(radians),1,0,_maxLength,0);
    } else {
      _phase = PHASE_DYING;
    }
    
  }
  
  
  
  
  /*
  *  CHILD SPAWNING
  */
  
  this.childSpawning = function() {
    
    if (_phase == PHASE_HOLDING) {
      
      if (random(1.0) < SETTINGS.CHILD_SPAWN_CHANCE_PER_FRAME) {
        var child = spawnBrickChild(_this);
        return child;
      }
      
    }
    
    return null;
  }
  
  
  
  
  
  
  /*
  *  DRAWING
  */  
  
  this.draw = function() {
    //console.log(_phase+" "+(_phase == PHASE_DYING));
    //console.log(this.isDying());
    if (!this.isDying()) {
      push();
      translate(_x*SETTINGS.BRICK_BASE_SIZE,_y*SETTINGS.BRICK_BASE_SIZE,_z*SETTINGS.BRICK_BASE_SIZE);
      //scale(SETTINGS.BRICK_BASE_SIZE);
      if (_orientation==SETTINGS.ORIENTATION_X) {
        box(_length*SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE);
      } else if (_orientation==SETTINGS.ORIENTATION_Y) {
        box(SETTINGS.BRICK_BASE_SIZE, _length*SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE);
      } else if (_orientation==SETTINGS.ORIENTATION_Z) {
        box(SETTINGS.BRICK_BASE_SIZE, SETTINGS.BRICK_BASE_SIZE, _length*SETTINGS.BRICK_BASE_SIZE);
      }
      //box(_w,_h,_d);
      pop();
    }
  }
  
  this.getPosition = function() {
    return [ _x, _y, _z ];
  }
  
  this.getOrientation = function() {
    return _orientation;
  }
  
  this.getLength = function() {
    return _length;
  }
  
  this.isDying = function() {
    return _phase == PHASE_DYING;
  }
  
  this.toString = function() {
    console.log("Brick");
  }
  
}

/*
*  BRICK TOOLS
*/

function makeRandomBrickLength() {
  var units = floor(random(SETTINGS.BRICK_LENGTH_UNITS_MIN,SETTINGS.BRICK_LENGTH_UNITS_MAX));
  return units;
}
