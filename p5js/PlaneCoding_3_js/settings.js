// SETTINGS


var SETTINGS;
function Settings() {
  this.CHILD_SIZE_MIN = random(0.9);
  this.CHILD_SIZE_MAX = random(this.CHILD_SIZE_MIN, 0.9);

  this.TREE_LEVEL_ROTATION_SPEED_INCREASE_MIN = random(1, 2);
  this.TREE_LEVEL_ROTATION_SPEED_INCREASE_MAX = this.TREE_LEVEL_ROTATION_SPEED_INCREASE_MIN + random(3);

  this.MAX_TREE_LEVELS = int(random(5, 10));

  this.MIN_COLOR_MORPH = random(0.1);
  this.MAX_COLOR_MORPH = random(0.5);

  this.ROT_OSCIL_MIN_FREQ = 0.2;
  this.ROT_OSCIL_MAX_FREQ = 1.5;

  this.ROT_OSCIL_AMP_MIN = random(0, 0.5);
  this.ROT_OSCIL_AMP_MAX = random(this.ROT_OSCIL_AMP_MIN, 1);

  this.ROT_OSCIL_CHILD_PHASE_OFFSET = random(0.01, 0.3);
  this.ROT_OSCIL_EXP = random(1, 5);

  this.FRAGMENTATION = random(0.8, 2);

  this.BLAST_SPEED = 8;
  this.AIR_DRAG = 0.99;

  // TYPES
  this.OSCILLATOR_SINEXP = 0;
  this.FUCKER_TYPE_BOX = 0;

  console.log("settings loaded");
}
