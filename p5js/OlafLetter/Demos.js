function AlphabetDemo() {

  //createDefaultLetterRenderProperties();

  var props = createDefaultLetterRenderProperties();
  var letterRenderer = new LetterRenderer(props);//100, 10, 0.05, 0.4);


  this.init = function() {
  }


  this.update = function() {
    if (mouseIsPressed) {
      letterRenderer.setPointVariation(map(mouseX, 0, width, 0, 0.1*10));
      letterRenderer.setCurveVariation(map(mouseY, 0, height, 0, 2*10));
      drawAll();
    }
  }

  function drawAll() {

    background(244);
    stroke(0);
    noFill();
    translate(20, 20);
    //for (var r=0; r<map(sin(map(frameCount,0,200,0,TWO_PI)),-1,1,1,10); r++) {
    var text = letters.ALL_LETTERS;

    for (var r=0; r<10; r++) {
      for (var i=0; i<text.length; i++) {
        letterRenderer.render(text[i]);
        if (letterRenderer.getXPos()>width-140) {
          letterRenderer.nextRow();
        }
      }
      letterRenderer.resetPosition();
    }
  }

  drawAll();
}




/*
*
 *
 *    TYPING DEMO
 *
 *
 */







function TypingDemo() {

  var props = createDefaultLetterRenderProperties();
  props[CHARACTER_HEIGHT] = 50;
  props[CHARACTER_SPACE] = 1;
  props[POINT_VARIATION] = 0.05;
  props[CURVE_VARIATION] = 0.2;

  var randomSeedInits = [ random(100), random(100) ];

  var letterRenderer = new LetterRenderer(props);
  var text = ""//TYPE ";

    this.init = function() {
    text = "";//TYPE ";
    drawAll();
  }

  this.keyTyped = function() {
    text += key;
    drawAll();
  }

  this.mousePressed = function() {
    drawAll();
  }

  this.update = function() {
    if (mouseIsPressed) drawAll();
  }

  function drawAll() {
    background(240);
    stroke(0);
    noFill();
    var arr = letters.createLetters(text);
    for (var l=0; l<2; l++) {
      //letterRenderer.resetPosition();
      letterRenderer.setRandomSeedInit(randomSeedInits[l]);
      letterRenderer.init();
      for (var i=0; i<arr.length; i++) {
        letterRenderer.render(arr[i]);
        if (letterRenderer.getXPos()>width-props[CHARACTER_HEIGHT]) {
          letterRenderer.nextRow();
        }
      }
    }
  }
}








/*
*
 *
 *    TYPING DEMO2
 *
 *
 */




function TypingDemo2() {

  var pointVariation0 = 5;
  var pointVariation1 = 0.02;
  var curveVariation0 = 5;
  var curveVariation1 = 0.1;
  
  var animSmooth = 0.1;
  
  var props = createDefaultLetterRenderProperties();
  props[CHARACTER_HEIGHT] = 57;
  props[CHARACTER_SPACE] = 8;
  props[POINT_VARIATION] = pointVariation0;
  props[CURVE_VARIATION] = curveVariation0;
  props[STROKE_COLOR] = color(240);
  props[STROKE_WEIGHT] = 5;

  var xPos = 20;
  var yPos = 20;
  //var randomSeedInits = [ random(100), random(100) ];

  var randomSeedInits = [];
  var letterRenderers = [];//new LetterRenderer(props);
  var text = ""//TYPE ";

    this.init = function() {
    text = "";//TYPE ";
    letterRendererers = [];
    randomSeedInits = [];
    drawAll();
  }

  this.keyTyped = function() {
    text += key;
    randomSeedInits.push( random(1000) );
    letterRenderers.push( new LetterRenderer(props) );
    drawAll();
  }

  this.mousePressed = function() {
    drawAll();
  }

  this.update = function() {
    if (mouseIsPressed)  { drawAll(); }
    for (var i=0;i<letterRenderers.length;i++) {
      var lr = letterRenderers[i];
      var pv = lr.getPointVariation();
      var deltaPv = pointVariation1-pv;
      pv += deltaPv*animSmooth;
      lr.setPointVariation(pv);
      var cv = lr.getCurveVariation();
      var deltaCv = curveVariation1-cv;
      cv += deltaCv*animSmooth;
      lr.setCurveVariation(cv);
      //console.log(pv);
    }
    drawAll();
  }

  function drawAll() {
    background(16);
    stroke(0);
    noFill();

    var x = xPos;
    var y = yPos;
    var arr = letters.createLetters(text);
    if (arr.length>0) {
      //for (var l=0; l<2; l++) {
      //letterRenderer.resetPosition();


      for (var i=0; i<arr.length; i++) {
        var letterRenderer = letterRenderers[i];
        letterRenderer.setRandomSeedInit(randomSeedInits[i]);
        letterRenderer.init();
        letterRenderer.setXPos(x);
        letterRenderer.setYPos(y);
        letterRenderer.render(arr[i]);
        if (letterRenderer.getXPos()>width-props[CHARACTER_HEIGHT]) {
          letterRenderer.nextRow();
          letterRenderer.setXPos(xPos);
        }
        x = letterRenderer.getXPos();
        y = letterRenderer.getYPos();
      }
      //}
    }
  }
}
