// arrays: multiple dots are drawn where the mouse is and are fading out over time

float[] x = new float[50];
float[] y = new float[50];

void setup() {
  size(555, 666);
}

void mousePressed() {
  saveFrame("Capture-#####.jpg");
}

void draw() {
  background(0);
  // draw the positions
  for (int i=0; i<x.length; i=i+1) {
    float opacity = map(i, 0, x.length-1, 255, 0);
    fill(255,opacity);
    ellipse(x[i], y[i], 10, 10);
    
  }

  // shift the array right ..
  for ( int index = x.length-2; index >= 0; index=index-1 ) {
    x[index+1] = x[index];
    y[index+1] = y[index];
  }

  // .. and put mouse position at beginning of array
  x[0] = mouseX;
  y[0] = mouseY;
  
  //saveFrame("MyFrame-####.jpg");
  //save("MyPicture.jpg");
}
