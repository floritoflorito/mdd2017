
var mainRotOscilFreq;
var mainFucker;
var allFuckers = new Array();

function setup() {
  createCanvas(800, 600, WEBGL);
  SETTINGS = new Settings();
  
  noStroke();
  mainRotOscilFreq = random(SETTINGS.ROT_OSCIL_MIN_FREQ, SETTINGS.ROT_OSCIL_MAX_FREQ);
  
  var col = 0xFFCD8ECE;
  col = color(random(255), random(255), random(255));
  console.log("color "+col);
  mainFucker = new Fucker(
    0, 0, 0, 
    SETTINGS.FUCKER_TYPE_BOX, 
    [ width/10*random(1), width/10*random(1), width/10*random(1) ], 
    col, 0, mainRotOscilFreq, 0);
  mainFucker.detached = false;
  allFuckers.push(mainFucker);
  
  //console.log(mainFucker);
  //console.log(mainFucker.id);
  //console.log(allFuckers);
}



/*
function mousePressed() {
  createAndAttachChild();
  console.log("allFuckers: "+allFuckers.length);
}
*/

function mousePressed() {
  detachChild();
}

function draw() {
  
  if (allFuckers.length<1000) {
    var count = floor(random(1, 5));
    for (var i=0; i<count; i++)
      createAndAttachChild();
  }

  /*
  if (allFuckers.length>100 && noise(frameCount*0.04)<0.4) {
    detachChild();
  }
  */
  
 /*
  if (mouseIsPressed) {
    for (var i=0; i<10; i++)
      createAndAttachChild();
  }
  */
  

  background(244, 244, 255);
  

  noStroke();

  ambientLight(128, 128, 138) ;
  directionalLight(192, 192, 200, -0.4, -0.2, -1);
  //lightFalloff(1, 0, 0);
  //lightSpecular(0, 0, 0);

  camera(map(mouseX,0,width,-800,800),0,map(mouseY,0,height,200,1800),0,0,0,0,1,0);

  push();

  for (var i=0; i<allFuckers.length; i++)
    allFuckers[i].update();

  for (var i=0; i<allFuckers.length; i++) {
    var f = allFuckers[i];
    if (f.readyToRemove) {

      var all = f.getAllChildrenAndSelf();
      for (var j=0; j<all.length; j++) {
        console.log("Remove "+all[j]);
        allFuckers.remove(all[j]);
      }
      i--;
    }
  }

  for (var i=0; i<allFuckers.length; i++) {
    if (allFuckers[i].detached) {
      allFuckers[i].drawAttached();
    }
  }

  mainFucker.drawAttached();


  pop();
}
