function randomPM() {
  return random(1)<0.5?-1:1;
}


function variateColor(col, maxPercentage) {
  var offR = random(255*maxPercentage)*randomPM();
  var offG = random(255*maxPercentage)*randomPM();
  var offB = random(255*maxPercentage)*randomPM();
  var r = max(0, min(255, floor( red(col) + offR )));
  var g = max(0, min(255, floor( green(col) + offG )));
  var b = max(0, min(255, floor( blue(col) + offB )));
  return color(r,g,b);
}



function sin01(rd) {
  return sin(rd)*0.5+0.5;
}


function sin01exp(rd, exp) {
  return pow(sin01(rd), exp);
}
