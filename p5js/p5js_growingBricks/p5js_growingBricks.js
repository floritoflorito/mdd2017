var SETTINGS = null;
var bricks = new Array();
var takenSpaces = new Array(); // array of xyz arrays

function setup() {
  createCanvas(400,400,WEBGL);
  SETTINGS = new Settings();
  //brick = new Brick(0,0,0,50,100,100);
  //console.log(brick);
  bricks.push( new Brick(0,0,0,makeRandomBrickLength(), floor(random(3)) ) );
}

function draw() {
  background(240);
  
  //var camX = map(sin(map(frameCount,0,100,0,TWO_PI)),-1,1,-400,400);
  camera(0,0,600,  0,0,0,  0,1,0);
  
  rotateX(radians(15));
  rotateY(radians(frameCount/2));
  
  // CHILD SPAWNING
  for (var i=0;i<bricks.length;i++) {
    var brick = bricks[i];
    var child = brick.childSpawning();
    if (child!=null) {
      bricks.push(child);
    }
  }
  
  
  // SIZE UPDATE
  for (var i=0;i<bricks.length;i++) {
    var brick = bricks[i];
    brick.sizeUpdate();
    if (brick.isDying()) {
      bricks.splice(i,1);
      i--;
    }
  }
  
  
  
  // DRAWING
  for (var i=0;i<bricks.length;i++) {
    var brick = bricks[i];
    brick.draw();
  }
  
}
