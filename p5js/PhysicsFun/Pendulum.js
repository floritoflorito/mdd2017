// in meters
function Pendulum(cordLength, startAngleInDegrees) {

  var g = 9.81;
  var angle = radians(startAngleInDegrees);
  var lastTime = millis();
  var angularVelocity = 0;

  this.start = function() {
    angle = radians(startAngleInDegrees);
    lastTime = millis();
    angularVelocity = 0;
  }


  //@see https://en.wikipedia.org/wiki/Pendulum_(mathematics)
  this.update = function() {

    var now = millis();
    var dt = (now - lastTime)/1000;
    lastTime = now;
    
    var acceleration = -g * sin(angle);
    angularVelocity = angularVelocity + acceleration * dt;
    var deltaPositionOnCurve = angularVelocity * dt;
    var fullCircle = 2*PI*cordLength;
    var circlePartial = deltaPositionOnCurve/fullCircle;
    var deltaAngle = TWO_PI * circlePartial;
    angle += deltaAngle;
  }


  this.draw = function(factor, xPos) {

    var top = 30;

    push();
    translate(xPos, top);
    
    strokeWeight(5);
    point(0,0);
    
    rotate(angle);
    point(0,cordLength*factor);
    strokeWeight(1);
    line(0, 0, 0, cordLength*factor);
    pop();
  }
}
