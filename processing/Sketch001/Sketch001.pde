// this will generate random coloured screens and random numbers into the console at 3 frames per second

// This runs only once
void setup() {
  size(800,600);
  background(255,128,0);
  println("start");
  frameRate(3);
}
// This runs all the time
void draw() {
  println("go");
  println(        random(200) );
  background( random(255), 128, 0 );
}
