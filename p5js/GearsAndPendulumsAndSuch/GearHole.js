function GearHole(gear, circlePercentage, distanceFromEdge, weight) {
  
  //console.log("new GearHole for ");
  //console.log(gear);
  
  this.drawLocal = function() {
    
    var r =  gear.getInnerRadius() - distanceFromEdge;
    
    noFill();
    stroke(0);
    strokeWeight(weight);
    push();
      rotate(circlePercentage*TWO_PI);
      translate(r,0);
      point(0,0);
    pop();
    
  }
  
  this.getDistanceFromEdge = function() { return distanceFromEdge; }
  this.getCirclePercentage = function() { return circlePercentage; }
}
